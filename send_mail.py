from dataclasses import dataclass
import dataclasses
from time import sleep
from typing import Any, Optional, Type, Union, Generator, Dict, Tuple
from mailmerge import MailMerge
import openpyxl
from pathlib import Path
from docx2pdf import convert
import emails
from jinja2 import Environment, FileSystemLoader, select_autoescape, Template
import yaml
from config import Config, config_factory


def get_value_generator(xlsx_db_path: Union[str, Path], sheet_name: Optional[str] = None):
    book = openpyxl.load_workbook(xlsx_db_path, data_only=True, read_only=True)
    if sheet_name is None:
        sheet = book.active
    else:
        sheet = book[sheet_name]

    headers = [sheet.cell(row=1, column=i+1).value
               for i in range(sheet.max_column)]
    headers_dict = {str(headers[i]): i+1 for i, header in enumerate(
        headers) if header is not None and str(header)}

    def generator():
        for row in range(2, sheet.max_row+1):
            print(f"row {row} / {sheet.max_row}")
            yield {header: str(sheet.cell(row=row, column=header_col).value) for header, header_col in headers_dict.items()}
    # Define the templates - assumes they are in the same directory as the code
    return generator


def write_docs(
        value_generator: Generator[Dict[str, str], None, None],
        settings: Config):
    # Merge in the values
    # pour inserer les variables dans Word, aller dans : inserer/ Champ... / Publipostage/ ChampFusion ( MERGEFIELD )
    # Créer un champ et dans le codes de champs écrire "MERGEFIELD NompChamp"
    # Show a simple example

    output_dir_doc = Path(settings.output_dir_doc)
    output_dir_doc.mkdir(parents=True, exist_ok=True)

    output_documents = []
    for i, values in enumerate(value_generator()):
        if not settings.data.filename_column in values:
            values[settings.data.filename_column] = f"Document merged {i}"
        output_doc_filename = make_doc(
            settings.templates.template_doc, values, output_dir_doc, settings.data.filename_column, settings.override_pdf)
        output_documents.append({
            "values": values,
            "path_doc": output_doc_filename,
            "filename": output_doc_filename.with_suffix("").name
        })
    return output_documents


def send_mail(
        template_html: Template,
        attachement_path: Union[Path, str],
        attachement_name: str,
        mail_to: Tuple[str, str],
        values: Dict[str, Any],
        settings: Config):
    email = emails.Message(
        subject=settings.templates.mail_subject_template,
        html=template_html, mail_from=(
            settings.mail.mail_from_name, settings.mail.mail_from), mail_to=mail_to)

    email.attach(filename=str(attachement_name),
                 content_disposition="attachment", data=open(attachement_path, "rb"))

    response = email.send(to=mail_to, render=values,
                          mail_from=settings.mail.mail_from, smtp=dataclasses.asdict(settings.mail.smtp))
    if response.status_code not in (250, ):
        print(
            f"Fail to send {attachement_path} to {values[settings.mail.mail_to_column]} with code {response.status_code}")


def make_doc(document_path: Union[str, Path], values: Dict[str, Any], output_dir: Union[str, Path], filename_column: str, override: bool):
    document = MailMerge(document_path)
    merge_fields = document.get_merge_fields()

    value_dict = {merge_field: values[merge_field]
                  for merge_field in merge_fields}
    output_file = values[filename_column] + ".docx"

    output_file = output_dir / output_file
    if not output_file.exists() or override:
        document.merge(**value_dict)
        document.write(output_file)

    return output_file


def main():
    settings = Config.from_setting_file("settings.yaml")

    generator = get_value_generator(
        settings.data.xlsx_file, settings.data.xlsx_sheet)
    jinja_env = Environment(
        loader=FileSystemLoader("."),
        autoescape=select_autoescape()
    )
    email_template = jinja_env.get_template(
        settings.templates.template_mail_html)

    doc_infos = write_docs(generator, settings)

    output_dir_pdf_path = Path(settings.output_dir_pdf)
    output_dir_pdf_path.mkdir(exist_ok=True, parents=True)

    for i, doc_info in enumerate(doc_infos):
        print(f"send {i+1} on {len(doc_infos)}")
        doc_info["pdf_name"] = doc_info["filename"] + ".pdf"
        doc_info["path_pdf"] = output_dir_pdf_path / doc_info["pdf_name"]
        if settings.override_pdf:
            doc_info["path_pdf"].unlink(missing_ok=True)

        if not doc_info["path_pdf"].exists():
            convert(str(doc_info["path_doc"]), str(doc_info["path_pdf"]))
        sleep(settings.delay_mail)
        send_mail(
            email_template,
            doc_info["path_pdf"],
            doc_info["pdf_name"],
            (doc_info["values"][settings.mail.mail_to_name_column] if settings.mail.mail_to_name_column is not None else "",
             settings.mail.test_mode_mail_to if settings.mail.test_mode else doc_info["values"][settings.mail.mail_to_column]),
            doc_info["values"],
            settings)


if __name__ == "__main__":
    main()

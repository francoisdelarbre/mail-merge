
from dataclasses import dataclass
import dataclasses
from typing import Optional, Type
import yaml


@dataclass
class Config:
    output_dir_doc: str
    output_dir_pdf: str

    @dataclass
    class TemplateConfig:
        template_doc: str
        template_mail_html: str
        mail_subject_template: str
    templates: TemplateConfig

    @dataclass
    class DataConfig:
        xlsx_file: str
        xlsx_sheet: str
        filename_column: str = "filename"
    data: DataConfig

    @dataclass
    class MailConfig:
        @dataclass
        class SMTPConfig:
            host: str
            port: str
            user: str
            ssl: bool
            password: str
            tls: bool
        smtp: SMTPConfig

        mail_from: str
        mail_from_name: str
        mail_to_column: str = "Mail"
        mail_to_name_column: Optional[str] = None
        test_mode: bool = False
        test_mode_mail_to: Optional[str] = None

    mail: MailConfig
    override_pdf: bool = False
    delay_mail: float = 0.25

    @classmethod
    def from_setting_file(cls, setting_filename: str):
        settings_dict = cls.read_setting_file(
            setting_filename=setting_filename)
        return config_factory(cls, settings_dict)

    @classmethod
    def read_setting_file(cls, setting_filename: str):
        with open(setting_filename, "r") as settings_file:
            settings = yaml.load(settings_file, yaml.CLoader)
        return settings


def config_factory(cls: Type, settings):
    if not dataclasses.is_dataclass(cls):
        try:
            return cls(settings)
        except Exception as e:
            if cls.__module__ == 'typing':
                for cls_type in cls.__args__:
                    if cls_type is not type(None):
                        try:
                            return cls_type(settings)
                        except Exception as e:
                            continue
            raise e
    kwargs = {}
    for field in dataclasses.fields(cls):
        if field.name in settings:
            kwargs[field.name] = config_factory(
                field.type, settings[field.name])

        elif field.default is dataclasses.MISSING and field.default_factory is dataclasses.MISSING:
            raise ValueError(
                "Missing field {field.name} for setting {cls}")
    return cls(**kwargs)
